const jwt = require('jsonwebtoken');

function Auth(req, res, next) {
    const header = req.headers["authorization"];

    if (typeof header !== "undefined") {
        const bearer = header.split(" ");
        const token = bearer[1];

        jwt.verify(token, process.env.TOKEN_SECRET, async (err, authorizedData) => {
            if (err) {
                res.sendStatus(403);
            }
            else {
                req.authorizedData = authorizedData;
                next();
            }
        })
    } else {
        res.sendStatus(403);
    }
}

module.exports = Auth