//validation
const Joi = require('@hapi/joi');


const registerVal = data => {
    const schema = {
        userName: Joi.string().min(5).required(),
        password: Joi.string().min(6).required(),
        firstName: Joi.string().required(),
        lastName: Joi.string().required(),
        groupName: Joi.string().required(),
      };
    return Joi.validate(data, schema);
};

const loginVal = data => {
    const schema = {
        userName: Joi.string().min(6).required(),
        password: Joi.string().min(6).required(),
      };
    return Joi.validate(data,schema);
};

const mediaVal = data => {
  const schema = {
      name: Joi.string().required(),
      url: Joi.string().required(),
      type: Joi.string().valid("lihaskunto", "liikkuvuus", "temppu", "audio").required(),
      difficulty: Joi.number().min(1).max(3)
    };
  return Joi.validate(data,schema);
};

const locationVal = data => {
  const schema = {
      name: Joi.string().required(),
      description: Joi.string().required(),
      lat: Joi.string().required(),
      lon: Joi.string().required(),
      question: Joi.string(),
      a: Joi.string(),
      b: Joi.string(),
      c: Joi.string(),
      correctAnswer: Joi.string().valid("a", "b", "c"),
    };
  return Joi.validate(data,schema);
};

// module.exports.registerVal = registerVal;
// module.exports.loginVal = loginVal;
module.exports = {
  registerVal,
  loginVal,
  mediaVal,
  locationVal
}