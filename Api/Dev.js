const express = require("express");
const route = express.Router();
const Auth = require('../TokenVerification')
const User = require("../DB/User");
const UserProgress = require('../DB/UserProgress');
const Group = require("../DB/Group");

route.delete('/removeUser', Auth, async(req,res)=>{
    const user = await User.findById(req.authorizedData._id);
    try {
        await User.deleteOne({_id: user._id})
        await Group.updateOne({_id: user.groupID}, {$pull: {users: user._id}})
        await UserProgress.deleteOne({_id: user._id})
        res.status(200);
    } catch (error) {
        return res.json(e)
    }
})

module.exports = route;