const express = require("express");
const route = express.Router();
const Auth = require('../TokenVerification')
const { mediaVal } = require('../validation');
const Media = require("../DB/Media");

route.get('/:type?', Auth, getMedia) // lihaskunto | liikkuvuus | temppu | audio
route.post('/', addMedia)
route.post('/populate', populateDB)

async function getMedia(req, res, next) {
    let media;
    if(req.params.type) 
        media = await Media.find({type: req.params.type});
    else
        media = await Media.find({});

    res.json(media);
}
async function addMedia(req,res, next) {
    const { error } = mediaVal(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    const {name, url, type, difficulty} = req.body;

    let existingMedia = await Media.findOne({$or:[{name},{url}]})
    if(existingMedia) return res.send('Media already exists')

    let media = {
        name,
        url,
        type
    }
    if(difficulty) media.difficulty = difficulty;
    
    let mediaModel = new Media(media);
    let createdMedia = await mediaModel.save();
    res.json(createdMedia._id)
}

async function populateDB(req,res) {
    const data = [
        {
            name: "Alaspäin katsova koira",
            url: "2IRF8g60ty0",
            type: "liikkuvuus",
            difficulty: 2,
        },
        {
            name: "Pohjelihasten venytys",
            url: "SrU6hA0N5uQ",
            type: "liikkuvuus",
            difficulty: 2,
        },
        {
            name: "Alaselän kierto",
            url: "ZUgvyaXIpbI",
            type: "liikkuvuus",
            difficulty: 3,
        },
        {
            name: "Jalat auki-eteen-sivuille -taivutukset",
            url: "qzRCRfpmKHY",
            type: "liikkuvuus",
            difficulty: 3,
        },
        {
            name: "Kissa-lehmä-liike",
            url: "s7pTkTDaYKM",
            type: "liikkuvuus",
            difficulty: 2,
        },
        {
            name: "Kylkivenytys",
            url: "fLhVRqOIZ0c",
            type: "liikkuvuus",
            difficulty: 1,
        },
        {
            name: "Lonkankoukistajat ja takareiden joustot",
            url: "HEifYihChEk",
            type: "liikkuvuus",
            difficulty: 3,
        },
        {
            name: "Pakaravenytys istuen",
            url: "qqpM23QR2Oc",
            type: "liikkuvuus",
            difficulty: 2,
        },
        {
            name: "Rintakehän avaus",
            url: "-TDNjbRdfXo",
            type: "liikkuvuus",
            difficulty: 1,
        },
        {
            name: "Rintarangan kierto",
            url: "WtSs_ArM7mo",
            type: "liikkuvuus",
            difficulty: 1,
        },
        {
            name: "Rintarangan liikkuvuus kepillä",
            url: "f3q8yYOEEbs",
            type: "liikkuvuus",
            difficulty: 1,
        },
        {
            name: "Sormien nyrkistys ja ojennus",
            url: "cm-iVmNRZbM",
            type: "liikkuvuus",
            difficulty: 1,
        },
        {
            name: "Suorin jaloin istuminen",
            url: "RGXE0OjTuDo",
            type: "liikkuvuus",
            difficulty: 1,
        },
        {
            name: "Yläselän halaus",
            url: "_rWessz_CYs",
            type: "liikkuvuus",
            difficulty: 1,
        },
        {
            name: "Lantionnosto",
            url: "6JeDvfhEdyI",
            type: "lihaskunto",
        },
        {
            name: "Kyykistys ja polvennosto",
            url: "SrU6hA0N5uQ",
            type: "lihaskunto",
        },
        {
            name: "Ojentajakävely",
            url: "haXzUhw9f2o",
            type: "lihaskunto",
        },
        {
            name: "Ojentajalihasten venytys",
            url: "lUD3OL8OfUA",
            type: "liikkuvuus",
            difficulty: 1
        },
        {
            name: "Pomppupallo",
            url: "NYd9D66KgHU",
            type: "temppu",
        },
        {
            name: "Rullaus selällään",
            url: "k8F88Puy2cI",
            type: "liikkuvuus",
            difficulty: 2
        },
        {
            name: "Seuraa johtajaa",
            url: "Bb_QTi2Wk7Y",
            type: "temppu",
        },
        {
            name: "Ympyrää ja viivaa",
            url: "fQ893EampjE",
            type: "temppu",
        },
        {
            name: "AXY hyppy",
            url: "1lHtCbYoZVU",
            type: "temppu",
        },
        {
            name: "Dead bug",
            url: "Etw8vO-u3Z8",
            type: "lihaskunto",
        },
        {
            name: "Käsikasi",
            url: "nc_BUmuARZg",
            type: "temppu",
        },
        { 
            name: "Punnerrus", 
            url: "AnCMlECiv1E",
            type: "lihaskunto",
        },
        { 
            name: "Kupinkääntöliike", 
            url: "h6th7TD4G8U",
            type: "lihaskunto"
        },
        { 
            name: "Käsien nostot selkä seinää vasten", 
            url: "xl9MZADymJ0",
            type: "lihaskunto"
        },
        { 
            name: "Ajattele jotain positiivista tästä päivästä", 
            url: "MkqCnsftUkc",
            type: "audio",
        },
        { 
            name: "Asteittain rentoutus", 
            url: "Cpzctydx-IY",
            type: "audio"
        },
        { 
            name: "Tietoinen hengittaminen", 
            url: "XJHmpLekUu0",
            type: "audio"
        },
        { 
            name: "Mieti jotain mistä pidät itsessäsi", 
            url: "7yeNssREa14",
            type: "audio"
        },
        { 
            name: "JännitysRentoutus", 
            url: "--9vAMrT-aw",
            type: "audio"
        },
        { 
            name: "Huomion siirtäminen omaan kehoon", 
            url: "wxPnk6mv2Dw",
            type: "audio"
        },
        { 
            name: "Hengitysharjoitus", 
            url: "-uWtAvNPBRU",
            type: "audio"
        }
    ];
    data.forEach(async (val) => {
        let media = val;
        if(await Media.findOne({name: val.name})) return;
        let mediaModel = new Media(media);
        await mediaModel.save();
    })
    res.send('Media database populated');
}

module.exports = route;