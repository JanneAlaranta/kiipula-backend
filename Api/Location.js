const express = require("express");
const route = express.Router();
const multer  = require('multer');
const { locationVal } = require('../validation');
const Auth = require('../TokenVerification');
const IsMongoIdValid = require('../IsMongoIdValid');
const UserProgress = require('../DB/UserProgress');
const Locations = require("../DB/Location");

const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, process.env.IMAGE_PATH);
    },
    // change incoming filename to location name
    // replaces whitespaces with underscore
    filename: function (req, file, cb) {
        let extension = file.originalname.split('.').pop();
        let newName = `${req.body.name.replace(/\s+/g, '_')}.${extension}`
        cb(null , newName);
    }
});
const upload = multer({storage: storage})

route.get("/", Auth, getAllLocations);
route.get("/user", Auth, getUserLocations);
route.get("/:id", Auth, getLocationsById);
route.post("/", upload.single('photo'), addNewLocation);
route.put("/visited", Auth, visitLocation);
route.put("/answered", Auth, answerQuestion);

async function getAllLocations(req, res) {
    res.json(await Locations.find());
}

async function getUserLocations(req, res) {
    const userExists = await UserProgress.findOne({_id: req.authorizedData._id});
    if (!userExists) return res.sendStatus(403);

    res.json({
        locations: userExists.locationsVisited,
        answered: userExists.questionsAnswered
    }) 
}

async function getLocationsById(req, res) {
    if(!IsMongoIdValid(req.body.location)) 
        return res.status(400).send('Invalid location id')

    res.json(await Locations.findById(req.params.id))
}

async function addNewLocation(req, res) {
    const { error } = locationVal(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    const{name,description,lat,lon} = req.body;
    const activity = {
        question:req.body.question,
        answers: {
            a: req.body.a,
            b: req.body.b,
            c: req.body.c
        },
        correctAnswer: req.body.correctAnswer
    }
    
    let newLocation = {
        name,
        description,
        photo_url: req.file.filename,
        lat,
        lon,
        activity
    }
    
    let locationModel = new Locations(newLocation);
    let createdLocation = await locationModel.save();
    
    res.json(createdLocation)
}

async function visitLocation(req, res) {
    const userExists = await UserProgress.findOne({_id: req.authorizedData._id});
    if (!userExists) return res.sendStatus(403);

    if(!IsMongoIdValid(req.body.location)) 
        return res.status(400).send('Invalid location id')

    const isValidLocation = await Locations.findById(req.body.location);
    if(!isValidLocation) return res.status(400).send('Location not found')

    if(userExists.locationsVisited.includes(isValidLocation._id)) {
        return res.send('Location already visited');
    }

    userExists.locationsVisited.push(isValidLocation._id);
    await userExists.save();
    
    res.json(userExists.locationsVisited)  
} 

async function answerQuestion(req, res) {
    const userExists = await UserProgress.findOne({_id: req.authorizedData._id});
    if (!userExists) return res.sendStatus(403);

    if(!IsMongoIdValid(req.body.location)) 
        return res.status(400).send('Invalid location id')

    const isValidLocation = await Locations.findById(req.body.location);
    if(!isValidLocation) return res.status(400).send('Location not found');

    if(userExists.questionsAnswered.includes(isValidLocation._id)) {
        return res.send('Question already answered correctly');
    }

    userExists.questionsAnswered.push(isValidLocation._id);
    await userExists.save();

    res.json(userExists.questionsAnswered)
}

module.exports = route;