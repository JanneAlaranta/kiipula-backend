const express = require('express')
const mongoose = require('mongoose')
const route = express.Router();
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const {registerVal, loginVal} = require('../validation');
const Auth = require('../TokenVerification')
const User = require('../DB/User')
const UserProgress = require('../DB/UserProgress')
const Group = require('../DB/Group')
const dayjs = require('dayjs');
const weekOfYear = require('dayjs/plugin/weekOfYear');
const isBetween = require('dayjs/plugin/isBetween')
dayjs.extend(isBetween)
dayjs.extend(weekOfYear);
require('dayjs/locale/fi');
dayjs.locale('fi');

route.get('/getData', Auth, getUserData)
route.get('/allUsers', getAllUsers)
route.get("/userGroup", Auth, getUserGroup);
route.get('/allGroups', getAllGroups)
route.get('/leaderboard/:timeRange/', Auth, leaderboard)
route.post('/newUser', addUser);
route.post('/newGroup', addGroup)
route.post('/login', login)
route.put('/updateSteps', Auth, updateSteps)
route.put('/locationFound', Auth, locationFound)
route.put('/changeGroup', Auth, changeGroup)

async function getAllUsers(req,res){
    User.find(function (err, usrs) {
            // if (err) return console.error(err);
            res.json(usrs);
        })
}
async function addUser(req,res){
    //validoidaan rekisteröityminen, jotta jokainen vaadittu kenttä saadaan
    const { error } = registerVal(req.body);
    if (error) return res.status(400).send(error.details[0].message);
    
    //katotaan onko username tai password olemassa
    const userNameExists = await User.exists({userName: req.body.userName});
    if(userNameExists) return res.status(400).send('Username already exists');

    //salasanan kryptaus
    const salt = await bcrypt.genSalt(10);
    const hashPassword = await bcrypt.hash(req.body.password, salt);

    const{firstName,lastName,userName,password, groupName} = req.body;
    let user = {}
    // https://stackoverflow.com/questions/30822078/how-would-i-develop-a-relationship-user-belongs-to-groups-in-mongoose-node-js
    let requestedGroup = await Group.findOne({name: groupName});
    if(!requestedGroup) return res.status(400).send('Group not found')

    user.firstName = firstName;
    user.lastName = lastName;
    user.userName = userName;
    user.password = hashPassword;
    user.groupID = requestedGroup._id;

    let userModel = new User(user);
    let createdUser = await userModel.save();

    // luo ja lähetä token ettei tarvitse kirjautua tunnuksen luomisen jälkeen
    const token = jwt.sign({_id: createdUser._id}, process.env.TOKEN_SECRET);
    res.header('auth_token', token).send(token);
    
    requestedGroup.users.push(createdUser.id);
    await requestedGroup.save();

    // luodaan käyttäjälle pistetaulu jolla seurataan käyttäjän edistymistä
    let userProgress = {}
    userProgress._id = createdUser._id
    userProgress.totalPoints = 0
    userProgress.stepCollection = {
        totalSteps: 0,
        steps: new Array()
    }
    userProgress.locationsFound = [
        {name:"Tapahtumapuiston puuveistos",found:false},
        {name:"Lahden matkakeskuksen alikulkutunnelin maskit",found:false},
        {name:"Fellmanninpuiston muistomerkki",found:false},
        {name:"Hakkapeliittapatsas",found:false},
        {name:"Hiihtäjäpatsas",found:false},
        {name:"Hyppyrimäet",found:false},
        {name:"Häränsilmä",found:false},
        {name:"Kirkkopuiston patsas",found:false},
        {name:"Lanupuisto",found:false},
        {name:"Jari Litmasen Patsas",found:false},
        {name:"Paasikiven Patsas",found:false},
        {name:"Perhepuiston skeittirampit",found:false},
        {name:"Radiomastot",found:false},
        {name:"Radiomuseon eläimet",found:false},
        {name:"Tiirismaan lenkkipolku",found:false},
        {name:"Vesiurut",found:false}
    ]

    userProgress.locationsVisited = new Array();
    userProgress.questions = "false"
    let userProgressModel = new UserProgress(userProgress)
    await userProgressModel.save();
}

async function addGroup(req,res) {
    let existingGroup = await Group.exists({name: req.body.name})
    if(existingGroup) return res.status(400).send('Group name already exists')

    let group = {};
    group.name = req.body.name;
    group.users = new Array();


    let groupModel = new Group(group);
    let createdGroup = await groupModel.save();
    res.json({_id: createdGroup._id})
}

async function getUserGroup(req, res, next) {
    const userExists = await User.findOne({_id: req.authorizedData._id});
    if (userExists) {
        const grup = await Group.findById(userExists.groupID)
        res.json(grup.name)
    } else {
        res.sendStatus(403)
    }
}

async function getAllGroups(req, res, next) {
    Group.find(function (err, grp) {
        // if (err) return console.error(err);
        let ret = grp.map(v => v.name).sort((a,b) => {
            if(a < b) return -1;
            if(a > b) return 1;
            return 0
        })
        res.json(ret);
    })     
}

async function login(req,res) {
    const { error } = loginVal(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    const userExists = await User.findOne({userName: req.body.userName});
    if(!userExists) return res.status(400).send('Username or password is invalid');
    //salasanan tarkistus
    const validPass = await bcrypt.compare(req.body.password, userExists.password);
    if(!validPass) return res.status(400).send('Username or password is invalid');

   
    //jsonwebtokenin luonti
    const token = jwt.sign({_id: userExists._id}, process.env.TOKEN_SECRET);
    res.header('auth_token', token).send(token);

}

async function getUserData(req, res, next) {
    const userExists = await UserProgress.findOne({_id: req.authorizedData._id});
    if(userExists) return res.json(userExists);
    else return res.status(400).send('User not found'); 
}

async function locationFound(req, res, next) {
    const userExists = await UserProgress.findOne({_id: req.authorizedData._id});
    if(userExists){
        let elem = userExists.locationsFound.find(location => location.name === req.body.location)
        elem.found = true;
        elem.time = new Date().toISOString();
        userExists.totalPoints += 1000
        await userExists.save();
        return res.json(userExists);
    }
    else return res.status(400).send('User not found');
}

async function updateSteps(req, res, next) {
    const userExists = await UserProgress.findOne({_id: req.authorizedData._id});
    if(userExists){
        let date = new Date().toISOString().split('T')[0] //only date
        let arr = userExists.stepCollection.steps;
        if(arr[arr.length-1] !== undefined && arr[arr.length-1].time === date) {
            arr[arr.length-1].steps += req.body.stepCount;
        } else {
            arr.push({
                steps: req.body.stepCount,
                time: date
            });
        }
        userExists.totalPoints += req.body.stepCount;
        userExists.stepCollection.totalSteps += req.body.stepCount;
        await userExists.save();
        return res.json(userExists);
    }
    else return res.status(400).send('User not found');
}

async function leaderboard(req, res, next) {
    if(!['day', 'week', 'month'].includes(req.params.timeRange)) return res.status(400).send('parameter must be one of following: day, week, month')

    const userExists = await User.findOne({_id: req.authorizedData._id});
    if(userExists){
        const userGroup = await Group.findOne({_id: userExists.groupID})
        const usersInGroup = await UserProgress.find({_id: {$in: userGroup.users}})

        let grouped;
        let unSorted = new Array();
        let inRange = new Array();
        let finalList = new Array();

        
        for (let user of usersInGroup) {
            let filter = new Array();
            let currentuser = await User.findOne({_id: user._id})
            let newProps = {
                username: currentuser.userName
            }
            if(user.stepCollection.steps.length == 0) continue;

            //filtering results for smaller subsequent loops
            for(let i = user.stepCollection.steps.length - 1; i >= 0; i--) {
                if (
                    dayjs(user.stepCollection.steps[i].time)
                    .isBetween(
                        dayjs().startOf(req.params.timeRange),
                        dayjs().endOf(req.params.timeRange),
                        null,
                        "[]"
                    )
                )
                    filter.push(user.stepCollection.steps[i]);
                //koska array on aika järjestyksessä, aikavälistä poistuessa lopetetaan looppaus
                else break;
            }

            //construct objects to return
            filter.forEach((val)=> {
                let temp = {
                    "_id": val._id,
                    "steps": val.steps,
                    "time": val.time
                }
                Object.assign(temp, newProps)
                unSorted.push(temp)
            })
        }

        if(unSorted.length == 0) return res.send("No records found");

        grouped = groupByTime(unSorted, req.params.timeRange)
        
        if(req.params.timeRange == 'day') {
            inRange = grouped[dayjs(new Date()).format('DD-MM')] ?? [];
        }
        else if(req.params.timeRange == 'week') {
            inRange = grouped[dayjs(new Date()).week()] ?? [];
        }
        else if(req.params.timeRange == 'month') {
            inRange = grouped[dayjs(new Date()).month() + 1] ?? [];
        }

        inRange.sort((a,b) => {
            return b.steps - a.steps
        })

        // filter out duplicate people
        let inList = {};

        inRange.forEach((val) => {
            if(inList[val.username]) return;
            inList[val.username] = true;
            finalList.push(val);
        })

        if (finalList.length > 10) finalList.length = 10;
        res.json(finalList)
        
    } else res.status(400).send('user not recognised')
}

async function changeGroup(req,res) {
    const user = await User.findById(req.authorizedData._id);
    const userGroup = await Group.findById(user.groupID);
    const requestedGroup = await Group.findOne({name: req.body.name});

    const index = userGroup.users.indexOf(user._id);
    if (index > -1) {
        userGroup.users.splice(index, 1);
    }
    user.groupID = requestedGroup._id;
    requestedGroup.users.push(user._id);

    await user.save();
    await userGroup.save();
    await requestedGroup.save();

    res.sendStatus(200)
}

function groupByTime(list, timeRange = 'day') {
    // https://stackoverflow.com/questions/20630676/how-to-group-objects-with-timestamps-properties-by-day-week-month/38896252
    let toReturn = {};

    for(let item of list) {
        let paramName = dayjs(item.time).format('DD-MM');
        if(timeRange == "week") paramName = dayjs(item.time).week();
        else if(timeRange == "month") paramName = dayjs(item.time).month() + 1;

        if(toReturn[paramName] == null) {
            toReturn[paramName] = [];
        }
        toReturn[paramName].push(item);
    }

    return toReturn;
}
module.exports = route;