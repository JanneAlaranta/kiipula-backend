/*
let firstUserID = '5b360fdea392d731829ded18';
let secondUserID = 'aaaaaaaaaaaa';

console.log(mongoose.Types.ObjectId.isValid(firstUserID));  true
console.log(mongoose.Types.ObjectId.isValid(secondUserID)); true

let checkForValidMongoDbID = new RegExp("^[0-9a-fA-F]{24}$");
console.log(checkForValidMongoDbID.test(firstUserID));  true
console.log(checkForValidMongoDbID.test(secondUserID)); false 
*/

function IsMongoIdValid(id) {
    return id.match(/^[0-9a-fA-F]{24}$/);
}

module.exports = IsMongoIdValid