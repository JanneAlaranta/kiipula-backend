/*
const express = require('express')
const cors = require('cors')
const connectDB = require('./DB/Connection')
const dotenv = require("dotenv");

dotenv.config();

const app = express()

app.use(cors())
const port = 8080

// connect to MongoDB
connectDB()

app.use(express.json({extended:false}))
app.use('/api/userModel', require('./Api/User'))
app.use('/api/locationModel', require('./Api/Location'))
app.use('/api/mediaModel', require('./Api/Media'))
app.use('/api/dev', require('./Api/Dev'))

//serve content from /images to url /photo
app.use('/photos', express.static(__dirname + '/images'));


app.listen(port, () => {
  console.log(`Kiipula backend app listening at http://localhost:${port}`)
})
*/


const express = require('express')
var cors = require('cors')
const connectDB = require('./DB/Connection')
const fs = require('fs');
const http = require('https')
const dotenv = require("dotenv");

dotenv.config();

var key = fs.readFileSync(process.env.KEY);
var cert = fs.readFileSync(process.env.CERT);
var options = {
  key: key,
  cert: cert
};

const app = express()

app.use(cors())
const port = 443

// connect to MongoDB
connectDB()

app.use(express.json({extended:false}))
app.use('/api/userModel', require('./Api/User'))
app.use('/api/locationModel', require('./Api/Location'))
app.use('/api/mediaModel', require('./Api/Media'))
app.use('/api/download', function (req, res) {
  res.download('/var/www/public/sportspace.apk');
})

//serve content from IMAGE_PATH to url /photo
app.use('/photos', express.static(process.env.IMAGE_PATH));

var server = http.createServer(options, app);

server.listen(port, () => {
  console.log(`Kiipula backend app listening at https://sportspace.lab.fi:${port}`)
})


