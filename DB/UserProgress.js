const mongoose = require("mongoose");

const userProgress = new mongoose.Schema({
	_id: {
		type: mongoose.Schema.Types.ObjectId,
		required: true,
	},
	totalPoints: {
		type: Number,
		required: true,
	},
	stepCollection: {
		totalSteps: { type: Number, required: true },
		steps: [
			{
				steps: { type: Number, required: true },
				time: { type: String, required: true },
			},
		],
	},
	locationsFound: {
		type: [
			{
				name: { type: String, required: true },
				found: { type: Boolean, required: true },
				time: { type: String, required: false },
			},
		],
	},
	locationsVisited: [
		{
			type: mongoose.Schema.Types.ObjectId,
			ref: 'Locations'
		}
	],
	questionsAnswered: [
		{
			type: mongoose.Schema.Types.ObjectId,
			ref: 'Locations'
		}
	],
	questions: {
		type: String,
		required: true,
	},
});

module.exports = UserProgress = mongoose.model("userProgress", userProgress);
