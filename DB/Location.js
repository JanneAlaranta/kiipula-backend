const mongoose = require("mongoose");

const location = new mongoose.Schema({
	name: {
		type: String,
		required: true,
	},
	description: {
		type: String,
		required: true,
	},
    photo_url: {
		type: String,
	},
    lat: {
		type: String,
		required: true,
	},
    lon: {
		type: String,
		required: true,
	},
	activity: {
		question: {
			type: String
		},
		answers: {
			type: Object
		},
		correctAnswer: {
			type: String
		}
	}
});

// Location on jo varattu joten s perään
module.exports = Locations = mongoose.model("location", location);
