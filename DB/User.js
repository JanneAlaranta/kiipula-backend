const mongoose = require('mongoose')
const Group = require('./Group')

const user = new mongoose.Schema({
    firstName:{
        type:String,
        required:true,
    },
    lastName:{
        type:String,
        required:true,
    },
    userName:{
        type:String,
        required:true,
    },
    password:{
        type:String,
        required:true,
    },
    groupID: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Group'
    }
    
})

module.exports = User = mongoose.model('user', user)