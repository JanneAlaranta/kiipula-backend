const mongoose = require("mongoose");

// creating the connection to the db
const connectDB = async()=>{
    await mongoose.connect(process.env.DB_CONNECT,{
        useUnifiedTopology: true,
        useNewUrlParser: true })
        .then(()=>console.log('Connected to database!'))
        .catch((e)=>console.log('Something went wrong! ' + e))
}
module.exports = connectDB;
