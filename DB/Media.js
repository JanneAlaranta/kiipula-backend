const mongoose = require("mongoose");

const media = new mongoose.Schema({
	name: {
		type: String,
		required: true,
	},
    url: {
		type: String,
		required: true,
	},
    type: {
		type: String, //"lihaskunto" | "liikkuvuus"
		required: true,
	},
	difficulty: {
		type: Number
	}
});

// Location on jo varattu joten s perään
module.exports = Media = mongoose.model("media", media);
